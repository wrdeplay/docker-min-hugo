FROM ubuntu:bionic
MAINTAINER Simon Osborne <wrdeplay@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q \
    && apt-get install --no-install-recommends -qy \ 
    git \
    hugo \
    && rm -rf /var/lib/apt/lists/*

ENV HOME /data
WORKDIR /data
VOLUME ["/data"]


