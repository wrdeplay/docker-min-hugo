# Minimal image for hugo

`docker run --rm -i -u="$(id -u):$(id -g)" -p 1313:1313 -v "$PWD":/data wrdeman/docker-min-hugo hugo server -D --bind=0.0.0.0`
